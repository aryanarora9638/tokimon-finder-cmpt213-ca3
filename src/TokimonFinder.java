import Model.Gameplay;
import Model.PlayerInfo;
import UI.Grid;

import java.util.Scanner;

/**
 *This class takes user input for the number of toki and foki.
 *Creates the normal grid for the user to see and calls the gameplay method to start the game
 *@author aryanarora
 */

public class TokimonFinder {
    private static int numToki = -1;
    private static int numFoki = -1;
    private static boolean cheatMode = false;
    public static void main(String[] args){

        System.out.println("Argument length - " + args.length);

        //validate the arguments
        if(args.length > 3){
            System.out.println("Invalid Arguments");
            System.exit(-1);
        }
        else if(args.length == 0){
            numToki = 10;
            numFoki = 5;
        }
        else {//args is length 1, 2 or 3
            //check the arguments
            for(String arg : args){
                if(arg.contains("--numToki")){
                    getNumFromArg(arg);
                }
                else if(arg.contains("--numFoki")){
                    getNumFromArg(arg);
                }
                else if (arg.contains("--cheat")){
                    System.out.println("cheat mode enabled");
                    cheatMode = true;
                }
                else {
                    System.out.println("Invalid Arguments");
                    System.exit(-1);
                }
            }
            //args did not include any of toki or foki
            if(numToki == -1){
                numToki = 10;
            }
            if(numFoki == -1){
                numFoki = 5;
            }
            System.out.println("Num Toki - " + numToki);
            System.out.println("Num Foki - " + numFoki);

            if(numToki+numFoki > 100){
                System.out.println("Num of Toki and Foki Reached Maximum point");
                System.exit(-1);
            }
        }


        //Make Grid for the Game
        Grid grid = new Grid(10,10);

        //Gameplay
        Gameplay gameplay = new Gameplay(numToki, numFoki, cheatMode);
    }

    private static void getNumFromArg(String arg) {
        int startIndex  = arg.indexOf('=');
        int num = Integer.parseInt(arg.substring(startIndex+1, arg.length()));
        if(arg.contains("Toki")){
            numToki = num;
            if(numToki<5){
                System.out.println("Invalid Number of Tokimons");
                System.exit(-1);
            }
        }
        if(arg.contains("Foki")){
            numFoki = num;
            if(numFoki<5){
                System.out.println("Invalid Number of Fokimons");
                System.exit(-1);
            }
        }
    }

}
