package UI;

import java.util.ArrayList;

/**
 *The Grid class makes a matrix of cells and is used to print the user with the current status of the positions of the player and the toki and foki.
 *@author aryanarora
 */

public class Grid {
    private  int numRows;
    private  int numColumns;
    public  static Cell[][] cells;
    public static char[] rowLetter = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'};


    public Grid(int numRows, int numColumns) {
        this.numRows = numRows;
        this.numColumns = numColumns;
        cells = new Cell[numRows][numColumns];
        makeGridLayout();
        System.out.println("Grid Layout Build");
    }

    private void makeGridLayout() {
        for(int row = 0; row < cells.length; row++){
            for(int column = 0; column < cells.length; column++){
                cells[row][column] = new Cell();
            }
        }
    }

    public static void printGridLayout(){
        //Heading
        System.out.println();
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Game Grid: ");
        System.out.println();

        //Column Headings
        System.out.print("  0 1 2 3 4 5 6 7 8 9");
        System.out.println();

        //Game Grid
        for(int row = 0; row < cells.length; row++){
            //Row Headings
            System.out.print(rowLetter[row] + " ");
            for(int column = 0; column < cells.length; column++){
                System.out.print(cells[row][column].getCellStatus() + " ");
            }
            System.out.println();
        }
    }

    public static void printGridLayoutWithTokiFoki(){
        //Heading
        System.out.println();
        System.out.println("*-*-*-*-*-*-*-*-*-*-*-*");
        System.out.println("Game Grid: ");
        System.out.println();

        //Column Headings
        System.out.print("  0 1 2 3 4 5 6 7 8 9");
        System.out.println();

        //Game Grid
        for(int row = 0; row < cells.length; row++){
            //Row Headings
            System.out.print(rowLetter[row] + " ");
            for(int column = 0; column < cells.length; column++){
                System.out.print(cells[row][column].getCellStatusWithTokiFoki() + " ");
            }
            System.out.println();
        }
    }

    public static int getRowLetterIndex(Character inputLetter){
        int index = 0;
        while(rowLetter[index] != Character.toUpperCase(inputLetter)){
            index++;
            if(index > (rowLetter.length-1)){
                return -1;
            }
        }
        return index;
    }
}
