package UI;

/**
 * The Cell class represents the individual cell of the Grid matrix
 * it holds the status of each location on the grid i.e. contains any object like player, toki or foki
 * @author aryanarora
 */

public class Cell {
    private String cellStatus;
    private boolean isVisited;
    private boolean hasPlayer;
    private boolean hasToki;
    private boolean hasFoki;
    private boolean tokiCollected;

    public Cell(){
        cellStatus = "~";
        isVisited = false;
        tokiCollected = false;
        //checkStatus();
    }

    public String getCellStatus() {
        return cellStatus;
    }

    public String getCellStatusWithTokiFoki(){
        String value = "";
        if(this.isHasToki()){
            value = "$";
            if(this.isHasPlayer()){
                value = "@$";
            }
        }
        else if(this.hasFoki){
            value = "X";
            if(this.hasPlayer){
                value = "@X";
            }
        }
        else if(this.isVisited){
            value = " ";
            if(this.hasPlayer){
                value = "@";
            }
        }
        else if(this.isHasPlayer()){
            value = "@";
        }
        else{
            value = "~";
        }
        return value;
    }

    public void setCellStatus(String cellStatus) {
        this.cellStatus = cellStatus;
    }

    public void playerAtTheCell(){
        this.hasPlayer = true;
        if(this.isHasToki()){
            this.tokiCollected = true;
            this.setCellStatus("@$");
        }
        else {
            this.setCellStatus("@");
        }
    }

    public void playLeavingTheCell(){
        this.hasPlayer = false;
        if(this.isHasToki()){
            this.setCellStatus("$");
        }
        else {
            this.isVisited = true;
            this.setCellStatus(" ");
        }
    }

    public void tokiAtTheCell(){
        this.hasToki = true;
    }

    public void fokiAtTheCell(){
        this.hasFoki = true;
    }

    public void killfokiAtTheCell(){
        this.hasFoki = false;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }

    public boolean isHasPlayer() {
        return hasPlayer;
    }

    public boolean isHasToki() {
        return hasToki;
    }

    public boolean isHasFoki() {
        return hasFoki;
    }

    public boolean isTokiCollected() {
        return tokiCollected;
    }

    public void setTokiCollected(boolean tokiCollected) {
        this.tokiCollected = tokiCollected;
    }
}
