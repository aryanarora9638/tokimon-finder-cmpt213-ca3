package Model;

import UI.Grid;

/**
 * The PlayerInfo class records the position of the player and and updates the locations as well as the number of toki and foki caught
 * it also records the number of spells left and used by the user
 * @author aryanarora
 */

public class PlayerInfo {
    public static int playerRowPosition;
    public static int playerColumnPosition;
    public static int numTokiCollected;
    public static int numTokiCaught;
    public static int numFokiCollected;
    public static int numSpells;

    public PlayerInfo(String playerInitialPosition, int numToki, int numFoki){
        playerRowPosition = Grid.getRowLetterIndex(playerInitialPosition.charAt(0));
        playerColumnPosition = Integer.parseInt(playerInitialPosition.substring(1));
        numTokiCaught = 0;
        numTokiCollected = numToki;
        numFokiCollected = numFoki;
        numSpells = 3;
    }

    public void changePlayerLocation(String newPlayerLocation){
        setPlayerRowPosition(Grid.getRowLetterIndex(newPlayerLocation.charAt(0)));
        setPlayerColumnPosition(Integer.parseInt(newPlayerLocation.substring(1)));
    }

    public static int getPlayerRowPosition() {
        return playerRowPosition;
    }

    public static void setPlayerRowPosition(int playerRowPosition) {
        PlayerInfo.playerRowPosition = playerRowPosition;
    }

    public static int getPlayerColumnPosition() {
        return playerColumnPosition;
    }

    public static void setPlayerColumnPosition(int playerColumnPosition) {
        PlayerInfo.playerColumnPosition = playerColumnPosition;
    }

    public static int getNumTokiCollected() {
        return numTokiCollected;
    }


    public static void setNumTokiCollected(int numTokiCollected) {
        PlayerInfo.numTokiCollected = numTokiCollected;
    }

    public static int getNumFokiCollected() {
        return numFokiCollected;
    }

    public static void setNumFokiCollected(int numFokiCollected) {
        PlayerInfo.numFokiCollected = numFokiCollected;
    }

    public static int getNumSpells() {
        return numSpells;
    }

    public static void setNumSpells(int numSpells) {
        PlayerInfo.numSpells = numSpells;
    }

    public static int getNumTokiCaught() {
        return numTokiCaught;
    }

    public static void setNumTokiCaught(int numTokiCaught) {
        PlayerInfo.numTokiCaught = numTokiCaught;
    }
}
