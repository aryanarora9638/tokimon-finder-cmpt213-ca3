package Model;

import UI.Cell;
import UI.Grid;

import java.util.Random;
import java.util.Scanner;

/**
 * The Gameplay class the runs the whole game, in terms of taking in the user input and and moving the player position.
 * it notifies the UI (grid class) about the new positon, so that the Grid shows the real time position of the game to the user
 * @author aryanarora
 */

public class Gameplay {

    public static PlayerInfo playerInfo;
    private boolean hasWon;
    private boolean hasLost;
    private boolean isCheatMode;
    private Scanner input = new Scanner(System.in);
    private char[] allowedMoves = {'W', 'A', 'S', 'D'};
    private String[] spells = {"1. Jump to another Grid Location", "2. Reveal a toki Location", "3. Kill one Foki"};

    public Gameplay(int numToki, int numFoki, boolean cheatMode){

        //place Toki Foki Randomly
        placeTokiFokiOnTheGrid(numToki, numFoki);
        if(cheatMode){
            Grid.printGridLayoutWithTokiFoki();
        }
        else {
            Grid.printGridLayout();
        }

        //user location input
        String playerInitialPosition = getPlayerInitialPosition();

        //Stores user information
        playerInfo = new PlayerInfo(playerInitialPosition,numToki,numFoki);
        hasWon = false;
        hasLost = false;
        this.isCheatMode = cheatMode;

        //place the user
        placeTheUserOnTheGrid();

        //take user input to the game
        while(!gameOver()){
            userNextMove(playerInfo);
            placeTheUserOnTheGrid();
        }

        congratulationStatement();
    }

    private String getPlayerInitialPosition() {
        System.out.println();
        System.out.println("Enter Your initial Position");
        //get user input
        Scanner input = new Scanner(System.in);
        String initialPosition = "";
        System.out.print("-> ");
        initialPosition = input.next().trim();
        //validate the user input
        while(Grid.getRowLetterIndex(initialPosition.charAt(0)) == -1){
            System.out.println("Please Enter a Valid input");
            System.out.print("-> ");
            initialPosition = input.next().trim();
        }
        return initialPosition;
    }

    private void congratulationStatement() {
        if(hasLost){
            System.out.println();
            System.out.println("*-*-*-*-*-*-*-*-*-*-*-*");
            System.out.println("Sorry You Lost the Game");
            System.out.println("*-*-*-*-*-*-*-*-*-*-*-*");
            System.out.println();
        }
        if(hasWon){
            System.out.println();
            System.out.println("*-*-*-*-*-*-*-*-*-*-*-*");
            System.out.println("Congratulation You Won the Game");
            System.out.println("*-*-*-*-*-*-*-*-*-*-*-*");
            System.out.println();
        }
    }

    private void userNextMove(PlayerInfo playerInfo) {
        System.out.println();
        System.out.println("Toki Collected - " + playerInfo.getNumTokiCaught() + "/" + (playerInfo.getNumTokiCollected()+playerInfo.getNumTokiCaught()) + ", "
                + "Spells Left - " + playerInfo.getNumSpells() + "/3");
        System.out.println();
        System.out.print("Next Move (W,A,S,D to move or type spell to use ur spells) -> ");
        String userMove = input.next().trim();

        //spell
        if(userMove.equalsIgnoreCase("spells")){
            if(playerInfo.getNumSpells() != 0){
                printSpells();
                String spellSelection = input.next().trim();
                while (!validateSpell(spellSelection)){
                    System.out.println("Enter a Valid Spell Number!!");
                    System.out.print("->");
                    spellSelection = input.next().trim();
                }
                runSpell(spellSelection);
            }
            else{
                System.out.println("Sorry You used all your spells :(");
            }
        }
        //not spell, move instead
        else{
            //move one step
            int moveIndex = validateMove(userMove);
            while (moveIndex == -1){
                System.out.println("Invalid Input, Try Again");
                System.out.print("Next Move (W,A,S,D to move or type spell to use ur spells) -> ");
                userMove = input.next().trim();
                moveIndex = validateMove(userMove);
            }
            updatePlayerLocation(moveIndex);
        }
    }

    private boolean validateSpell(String spellSelection) {
        int num = 0;
        try{
            num = Integer.parseInt(spellSelection);
        }
        catch (NumberFormatException e){
            e.getMessage();
            return false;
        }

        if(num<0 || num>3){
            return false;
        }
        return true;
    }

    private void runSpell(String spellSelection) {
        int spellChoosen = Integer.parseInt(spellSelection);
        if(spellChoosen == 1){
            String newPlayerPosition = getPlayerInitialPosition();
            Grid.cells[playerInfo.getPlayerRowPosition()][playerInfo.getPlayerColumnPosition()].playLeavingTheCell();
            playerInfo.changePlayerLocation(newPlayerPosition);
            playerInfo.setNumSpells(playerInfo.getNumSpells()-1);
            placeTheUserOnTheGrid();
        }
        else if(spellChoosen == 2){
            System.out.println("Toki makes sound \'%\'");
            Random random = new Random();
            int randRow = random.nextInt(10);
            int randCol = random.nextInt(10);
            //find unique(different every time) toki
            while(!Grid.cells[randRow][randCol].isHasToki() && Grid.cells[randRow][randCol].getCellStatus() != "%"){
                randCol = random.nextInt(10);
                randRow = random.nextInt(10);
            }
            Grid.cells[randRow][randCol].setCellStatus("%");
            playerInfo.setNumSpells(playerInfo.getNumSpells()-1);
        }
        else if(spellChoosen == 3){
            Random random = new Random();
            int randRow = random.nextInt(10);
            int randCol = random.nextInt(10);
            while(!Grid.cells[randRow][randCol].isHasFoki()){
                randCol = random.nextInt(10);
                randRow = random.nextInt(10);
            }
            Grid.cells[randRow][randCol].killfokiAtTheCell();
            System.out.println("Random Foki killed (! _ !)");
            playerInfo.setNumSpells(playerInfo.getNumSpells()-1);
        }
        else{
            System.out.println("Invalid Spell number, Choose between 1, 2 or 3");
        }
    }

    private void printSpells() {
        for(String spell : spells){
            System.out.println(spell);
        }
    }



    private void updatePlayerLocation(int moveIndex) {
        Grid.cells[playerInfo.getPlayerRowPosition()][playerInfo.getPlayerColumnPosition()].playLeavingTheCell();
        if(moveIndex == 0){
            if(playerInfo.getPlayerRowPosition() >= 1) {
                playerInfo.setPlayerRowPosition(playerInfo.getPlayerRowPosition()-1);
                return;
            }
        }
        else if (moveIndex == 2){
            if(playerInfo.getPlayerRowPosition() <= 8){
                playerInfo.setPlayerRowPosition(playerInfo.getPlayerRowPosition()+1);
                return;
            }
        }
        else if (moveIndex == 1){
            if(playerInfo.getPlayerColumnPosition() >= 1){
                playerInfo.setPlayerColumnPosition(playerInfo.getPlayerColumnPosition()-1);
                return;
            }
        }
        else if (moveIndex == 3){
            if(playerInfo.getPlayerColumnPosition() <= 8){
                playerInfo.setPlayerColumnPosition(playerInfo.getPlayerColumnPosition()+1);
                return;
            }
        }
        else {
            System.out.println("Invalid Player Move");
        }
        System.out.println("Invalid Player Move");
    }

    private int validateMove(String userMove) {
        for(int index = 0; index < allowedMoves.length; index++){
            if(userMove.equalsIgnoreCase(String.valueOf(allowedMoves[index]))){
                return index;
            }
        }
        return -1;
    }

    private boolean gameOver() {
        if(playerInfo.getNumTokiCollected() == 0){
            hasWon = true;
        }
        else {
            hasWon = false;
        }

        if(hasLost) {
            return (hasLost);
        }
        return hasWon;
    }

    public void placeTokiFokiOnTheGrid(int numToki, int numFoki) {
        Random random = new Random();
        int randRow;
        int randColumn;

        for(int toki = 0; toki < numToki; toki++){
            randRow = random.nextInt(10);
            randColumn = random.nextInt(10);
            while(Grid.cells[randRow][randColumn].isHasToki() || Grid.cells[randRow][randColumn].isHasFoki()){
                randRow = random.nextInt(10);
                randColumn = random.nextInt(10);
            }
            Grid.cells[randRow][randColumn].tokiAtTheCell();
        }

        for(int foki = 0; foki < numFoki; foki++){
            randRow = random.nextInt(10);
            randColumn = random.nextInt(10);
            while(Grid.cells[randRow][randColumn].isHasFoki() || Grid.cells[randRow][randColumn].isHasToki()){
                randRow = random.nextInt(10);
                randColumn = random.nextInt(10);
            }
            Grid.cells[randRow][randColumn].fokiAtTheCell();
        }
    }


    private void placeTheUserOnTheGrid() {
        int newRow = playerInfo.getPlayerRowPosition();
        int newColumn = playerInfo.getPlayerColumnPosition();
        System.out.println(newRow + " " + newColumn);
        //update UI

        if(Grid.cells[newRow][newColumn].isHasToki()){
            //new toki found
            if(!Grid.cells[newRow][newColumn].isTokiCollected()){
                playerInfo.setNumTokiCollected(playerInfo.getNumTokiCollected()-1);
                playerInfo.setNumTokiCaught(playerInfo.getNumTokiCaught()+1);
                System.out.println("--------------------------------");
                System.out.println("Congratulations, new Toki found");
                System.out.println("--------------------------------");
            }
            //else
                //repeated toki found
        }
        if(Grid.cells[newRow][newColumn].isHasFoki()){
            Grid.cells[newRow][newColumn].playerAtTheCell();
            System.out.println("----------------");
            System.out.println("Oops Foki found");
            System.out.println("----------------");
            Grid.printGridLayoutWithTokiFoki();
            this.hasLost = true;//lost game
            return;
        }
        Grid.cells[newRow][newColumn].playerAtTheCell();
        Grid.printGridLayout();
//        if(isCheatMode){
//            Grid.printGridLayoutWithTokiFoki();
//        }
//        else {
//            Grid.printGridLayout();
//        }
    }
}
